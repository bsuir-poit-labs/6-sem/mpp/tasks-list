import express from "express";
import taskController from "../controllers/taskController.js";

const taskRouter = express.Router();

taskRouter.get("/filter/:status", taskController.getTasksByFilter)

taskRouter.get("/:id", taskController.getTask);

taskRouter.put("/:id", taskController.updateTask);

taskRouter.delete("/:id", taskController.deleteTask);

taskRouter.get("/", taskController.showTasks);

taskRouter.post("/", taskController.createTask);


export default taskRouter;
