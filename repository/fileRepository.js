import db from "../config/dbConfig.js";

const SAVE_FILE_QUERY = 'INSERT INTO tasks_files (original_name, file_name, task_id) VALUES ($1, $2, $3)'
const DELETE_FILE_BY_FILE_NAME_QUERY = 'DELETE FROM tasks_files WHERE file_name = $1'

const saveFileData = function (fileData, taskId) {
    db.query(SAVE_FILE_QUERY, [fileData.originalname, fileData.filename, taskId], (err) => {
        if (err) {
            console.log(err);
        }
    });
};

const deleteFileByFileName = function (fileName) {
    db.query(DELETE_FILE_BY_FILE_NAME_QUERY, [fileName], (err) => {
        if (err) {
            console.log(err);
        }
    });
}

export default {
    saveFileData,
    deleteFileByFileName
}
