import db from "../config/dbConfig.js";

const GET_ALL_TASKS_QUERY = 'SELECT * FROM tasks ORDER BY id';
const SAVE_TASK_QUERY = 'INSERT INTO tasks (title, description, date, status) VALUES ($1, $2, $3, $4)';
const GET_TASK_BY_ID_QUERY = 'SELECT * FROM tasks WHERE id = $1';
const GET_FILES_BY_TASK_ID_QUERY = 'SELECT * FROM tasks_files WHERE task_id = $1'
const UPDATE_TASK_BY_ID_QUERY = 'UPDATE tasks SET title = $1, description = $2, date = $3, status = $4 WHERE id = $5';
const DELETE_TASK_BY_ID_QUERY = 'DELETE FROM tasks WHERE id = $1'
const DELETE_FILES_BY_TASK_ID_QUERY = 'DELETE FROM tasks_files WHERE task_id = $1'
const GET_TASKS_BY_FILTER_QUERY = 'SELECT * FROM tasks WHERE status = $1'

const saveTask = function (task) {
    db.query(SAVE_TASK_QUERY,
        [task.title, task.description, task.date, task.status],
        (error, result) => {
            if (error) {
                console.log(error);
            }
            task.id = result.insertId;
            return task;
        });
}

const getTaskById = async function (taskId) {
    let result = await db.query(GET_TASK_BY_ID_QUERY, [taskId]);
    let task = result.rows[0];

    result = await db.query(GET_FILES_BY_TASK_ID_QUERY, [taskId]);
    task.files = result.rows;

    return task;
}

const updateTaskById = function (taskId, task) {
    db.query(UPDATE_TASK_BY_ID_QUERY,
        [task.title, task.description, task.date, task.status, taskId],
        (error) => {
            if (error) {
                console.log(error);
            }
        });
}

const deleteTaskById = function (taskId) {
    db.query(DELETE_TASK_BY_ID_QUERY, [taskId], (error) => {
        if (error) {
            console.log(error);
        }
    });

    db.query(DELETE_FILES_BY_TASK_ID_QUERY, [taskId], (error) => {
        if (error) {
            console.log(error);
        }
    });
}

const getAllTasks = async function () {
    let res = await db.query(GET_ALL_TASKS_QUERY);
    return res.rows;
}

const getTasksByFilter = async function(filter) {
    let res = await db.query(GET_TASKS_BY_FILTER_QUERY, [filter]);
    return res.rows;
}

export default {
    getAllTasks,
    saveTask,
    getTaskById,
    updateTaskById,
    deleteTaskById,
    getTasksByFilter
}
