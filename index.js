import express from "express";
import hbs from "hbs";
import multer from "multer";
import bodyParser from "body-parser";
import {APP_PORT, DIRNAME} from './config/appConfig.js';
import taskRouter from "./routes/taskRouter.js";
import fileRouter from "./routes/fileRouter.js";
import path from "path";
import hbsHelpers from './views/hbsHelpers.js';
import status from "./entity/Status.js";

const app = express();

hbs.registerHelper("formatDate", hbsHelpers.formatDate);
app.set("view engine", "hbs");
hbs.registerPartials(DIRNAME + "/views/partials");

app.use(express.static(path.join(DIRNAME, 'assets')));
app.use(bodyParser.json({extended: false}));
app.use(multer({dest: "uploads"}).single("fileData"));

app.use("/file", fileRouter);
app.use("/task", taskRouter);

app.use("/", function (request, response) {
    response.render("home.hbs", {
        status
    });
});

app.use(function (request, response) {
    response.status(404).send("Not Found");
});

app.listen(APP_PORT);
console.log(`Сервер запущен по адресу http://localhost:${APP_PORT}`);
