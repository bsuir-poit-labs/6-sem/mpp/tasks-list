function addTask() {
    showPrompt("Добавить задачу", null, function (task) {
        if (task != null) {
            let jsonTask = JSON.stringify(task);

            let request = new XMLHttpRequest();
            request.open("POST", "/task");
            request.setRequestHeader("Content-Type", "application/json");
            request.onload = () => {
                loadTasks();
            }

            request.send(jsonTask);
        }
    });
}

function editTask(taskId) {
    let request = new XMLHttpRequest();
    request.open("GET", `/task/${taskId}`, true);
    request.send();

    request.onload = function () {
        let task = JSON.parse(request.response);
        task.date = new Date(task.date);

        showPrompt("", task, function (newTask) {
            if (newTask != null) {
                let jsonTask = JSON.stringify(newTask);

                let request = new XMLHttpRequest();
                request.open("PUT", `/task/${taskId}`);
                request.setRequestHeader("Content-Type", "application/json");
                request.onload = () => {
                    loadTasks();
                }

                request.send(jsonTask);
            }
        });
    }
}

function deleteTask(taskId) {
    if (confirm("Вы действительно хотите удалить задачу?")) {
        let request = new XMLHttpRequest();
        request.open("DELETE", `/task/${taskId}`);
        request.onload = () => {
            document.querySelector('#btnCloseModal').click();
            loadTasks();
        }

        request.send();
    }
}

function deleteFile(fileName, taskId) {
    let request = new XMLHttpRequest();
    request.open("DELETE", `/file/${fileName}`);
    request.onload = function () {
        editTask(taskId);
    }
    request.send();
}

function showPrompt(text, task, callback) {
    let form = document.getElementById('prompt-form');
    document.getElementById('prompt-message').innerHTML = text;
    let btnDelete = document.getElementById("btnDelete");

    if (task == null) {
        setDefaultValue();
    } else {
        valueToControls(task);
    }

    function setDefaultValue() {
        btnDelete.style.display = "none";

        form.elements.title.value = '';
        form.elements.description.value = '';
        form.elements.status.options.selectedIndex = -1;
        form.elements.date.value = offsetDate(new Date(Date.now()));

        document.getElementById('fileListDiv').style.display = 'none';
    }

    function valueToControls(task) {
        btnDelete.style.display = "block";
        btnDelete.onclick = function () {
            deleteTask(task.id);
        };

        form.elements.title.value = task.title;
        form.elements.description.value = task.description;
        form.elements.status.value = task.status;
        form.elements.date.value = offsetDate(task.date);

        updateFileList(task.files, task.id);

        fileForm.action = `/file/${task.id}`;
        fileForm.addEventListener('change', function () {
            let formData = new FormData(fileForm);
            let request = new XMLHttpRequest();
            request.open('POST', fileForm.action);
            request.onload = function () {
                editTask(task.id);
            }
            request.send(formData);
        })
    }

    function offsetDate(date) {
        date.setMilliseconds(3 * 60 * 60 * 1000);
        let isoStr = date.toISOString();
        return isoStr.substring(0, isoStr.length - 8);
    }

    function updateFileList(files, taskId) {
        let fileList = document.getElementById("fileList");

        while (fileList.firstChild) {
            fileList.removeChild(fileList.firstChild);
        }

        // ReactDOM.render(
        //     <React.StrictMode>
        //         <FileList files={files} taskId={taskId}/>/>
        //     </React.StrictMode>,
        //     fileList
        // );



        for (const file of files) {
            let hrefDownloadFile = createHrefDownloadFile(file.original_name, file.file_name);
            let btnDeleteFile = createBtnDeleteFile(file.file_name, taskId);
            let div = createFileDiv(hrefDownloadFile, btnDeleteFile);
            fileList.appendChild(div);
        }

        document.getElementById('fileListDiv').style.display = 'block';
    }

    function createHrefDownloadFile(originalName, fileName) {
        let textNode = document.createTextNode(originalName);

        let href = document.createElement("a");
        href.className = 'text-secondary text-bottom';
        href.href = `/file/${fileName}/${originalName}`;
        href.appendChild(textNode);

        return href;
    }

    function createBtnDeleteFile(fileName, taskId) {
        let icon = document.createElement("i");
        icon.className = 'bi bi-x';

        let btn = document.createElement("button");
        btn.className = 'close';
        btn.appendChild(icon);
        btn.onclick = function () {
            deleteFile(fileName, taskId);
        }

        return btn;
    }

    function createFileDiv(hrefDownloadFile, btnDeleteFile) {
        let div = document.createElement('div');
        div.className = 'row border rounded px-2 m-1';
        div.appendChild(hrefDownloadFile);
        div.appendChild(btnDeleteFile);

        return div;
    }

    form.onsubmit = function () {
        let title = form.elements.title.value;
        let description = form.description.value;
        let date = form.date.value;

        let selectedIndex = form.status.options.selectedIndex;
        let status = form.status.options[selectedIndex].text;

        complete({title, description, date, status});

        return false;
    };

    function complete(value) {
        document.onkeydown = null;
        document.querySelector('#btnCloseModal').click();
        callback(value);
    }

    document.onkeydown = function (e) {
        if (e.key === 'Escape') {
            complete(null);
        }
    };

    let lastElem = form.elements[form.elements.length - 1];
    let firstElem = form.elements[0];

    lastElem.onkeydown = function (e) {
        if (e.key === 'Tab' && !e.shiftKey) {
            firstElem.focus();
            return false;
        }
    };

    firstElem.onkeydown = function (e) {
        if (e.key === 'Tab' && e.shiftKey) {
            lastElem.focus();
            return false;
        }
    };

    form.elements.title.focus();
}
