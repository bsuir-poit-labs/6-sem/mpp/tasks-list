import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
export const DIRNAME = path.dirname(path.dirname(__filename));

export const APP_PORT = parseInt(process.env.APP_PORT || '8080', 10);

export const DB_USER = process.env.DB_USER || 'postgres';
export const DB_PASSWORD = process.env.DB_PASSWORD || 'postgres';
export const DB_HOST = process.env.DB_HOST || 'localhost';
export const DB_DATABASE = process.env.DB_DATABASE || 'tasks_db';
export const DB_PORT = parseInt(process.env.DB_PORT || '5432', 10);
