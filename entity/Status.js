const status = {
    BACKLOG: "BACKLOG",
    TO_DO: "TO DO",
    IN_PROGRESS: "IN PROGRESS",
    DONE: "DONE"
};

Object.freeze(status);

export default status;
