class FileList extends React.Component {
    constructor() {
        super();
    }

    render() {
        let files = this.props.files;
        let taskId= this.props.taskId;
        return (
            <div>
                {files.map(function (file) {
                    return <FileItem key={file.id} file={file} taskId={taskId}/>
                })}
            </div>
        );
    }
}





