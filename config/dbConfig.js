import pkg from 'pg';
const {Pool} = pkg;
import {DB_USER, DB_HOST, DB_DATABASE, DB_PASSWORD, DB_PORT} from "./appConfig.js";

const db = new Pool({
    user: DB_USER,
    host: DB_HOST,
    database: DB_DATABASE,
    password: DB_PASSWORD,
    port: DB_PORT
});

export default db;
