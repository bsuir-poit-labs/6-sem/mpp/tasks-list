export default {
    formatDate(date) {

        let dd = date.getDate();
        if (dd < 10) dd = '0' + dd;

        let MM = date.getMonth() + 1;
        if (MM < 10) MM = '0' + MM;

        let yyyy = date.getFullYear();

        let hh = date.getHours();
        if (hh < 10) hh = '0' + hh;

        let mm = date.getMinutes();
        if (mm < 10) mm = '0' + mm;

        return `${dd}.${MM}.${yyyy}  ${hh}:${mm}`;
    }
}
