class TaskItem extends React.Component {
    constructor() {
        super();
    }

    formatDate(date) {

        let dd = date.getDate();
        if (dd < 10) dd = '0' + dd;

        let MM = date.getMonth() + 1;
        if (MM < 10) MM = '0' + MM;

        let yyyy = date.getFullYear();

        let hh = date.getHours();
        if (hh < 10) hh = '0' + hh;

        let mm = date.getMinutes();
        if (mm < 10) mm = '0' + mm;

        return `${dd}.${MM}.${yyyy}  ${hh}:${mm}`;
    }

    render() {
        let task = this.props.task;
        return (
            <div>
                <div className="row justify-content-center">
                    <div className="col-md-10">
                        <div className="container border rounded bg-white">
                            <br/>
                            <div className="row">
                                <div className="col-md-7">
                                    <h5 className="text-dark">{task.title}</h5>
                                </div>
                                <div className="col-md-2 text-right">
                                    <p className="btn btn-light">{task.status}</p>
                                </div>
                                <div className="col-md-2 text-left">
                                    <p className="btn btn-info">{this.formatDate(new Date(task.date))}</p>
                                </div>
                                <div className="col-md-1 text-right">
                                    <button className=" btn btn" onClick={() => editTask(task.id)}
                                            id={`editTask${task.id}`}
                                            data-toggle="modal" data-target="#taskModal">
                                        <i className="bi bi-pencil">

                                        </i>
                                    </button>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-10">
                                    <p className="text-secondary">{task.description}</p>
                                </div>
                                <div className="col-md-1">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
            </div>
        );
    }
}
