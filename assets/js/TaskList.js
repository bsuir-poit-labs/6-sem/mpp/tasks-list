class TaskList extends React.Component {
    constructor() {
        super();
    }

    render() {
        let tasks = this.props.tasks;
        return (
            <div>
                {tasks.map(function (task) {
                    return <TaskItem key={task.id} task={task}/>
                })}
                <div className="row justify-content-center">
                    <div className="col-md-10">
                        <button className="btn btn-primary" id="btnAdd" onClick={() => addTask()}
                                data-toggle="modal"
                                data-target="#taskModal">
                            Добавить задачу
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}


