import express from "express";
import fileController from "../controllers/fileController.js";
const fileRouter = express.Router();

fileRouter.post("/:taskId", fileController.uploadFile);

fileRouter.get("/:fileName/:originalName", fileController.downloadFile);

fileRouter.delete("/:fileName", fileController.deleteFile);

export default fileRouter;
