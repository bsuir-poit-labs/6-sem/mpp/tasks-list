import Task from "../entity/Task.js";
import taskRepository from "../repository/taskRepository.js";

const createTask = function (request, response) {
    if (!request.body) {
        return response.sendStatus(400);
    }

    const title = request.body.title;
    const description = request.body.description;
    const date = new Date(Date.parse(request.body.date));
    const status = request.body.status;

    const task = new Task(title, description, date, status);
    taskRepository.saveTask(task);
    response.sendStatus(200);
};

const getTask = async function (request, response) {
    const taskId = parseInt(request.params.id);
    response.json(await taskRepository.getTaskById(taskId));
}

const updateTask = function (request, response) {
    if (!request.body) {
        return response.sendStatus(400);
    }

    const taskId = parseInt(request.params.id);

    const title = request.body.title;
    const description = request.body.description;
    const date = new Date(Date.parse(request.body.date));
    const status = request.body.status;

    const task = new Task(title, description, date, status);
    taskRepository.updateTaskById(taskId, task);
    response.sendStatus(200);
}

const deleteTask = function (request, response) {
    const taskId = parseInt(request.params.id);
    taskRepository.deleteTaskById(taskId);
    response.sendStatus(200);
}

const showTasks = async function (request, response) {
    response.json(await taskRepository.getAllTasks());
};

const getTasksByFilter = async function (request, response) {
    const filter = request.params.status;
    response.json(await taskRepository.getTasksByFilter(filter));
};

export default {
    createTask,
    getTask,
    showTasks,
    updateTask,
    deleteTask,
    getTasksByFilter
}
