import fs from "fs";
import mime from "mime";
import fileRepository from "../repository/fileRepository.js";
import {DIRNAME} from "../config/appConfig.js";

const downloadFile = function (request, response) {
    let fileName = request.params.fileName;
    let originalName = request.params.originalName;
    let filePath = `${DIRNAME}\\uploads\\${fileName}`;

    response.setHeader('Content-type', mime.getType(originalName));

    response.download(filePath, originalName);
};

const uploadFile = function (request, response) {
    const taskId = parseInt(request.params.taskId);
    let fileData = request.file;
    fileRepository.saveFileData(fileData, taskId);
    response.sendStatus(200);
};

const deleteFile = function (request, response) {
    const fileName = request.params.fileName;

    fileRepository.deleteFileByFileName(fileName);

    let filePath = `${DIRNAME}\\uploads\\${fileName}`;
    fs.unlink(filePath, (err) => {
        if (err) {
            console.log(err);
        }
    });

    response.sendStatus(200);
};

export default {
    downloadFile,
    uploadFile,
    deleteFile
}
