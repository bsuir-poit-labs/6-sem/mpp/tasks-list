class FileItem extends React.Component {
    constructor() {
        super();
    }

    render() {
        let file = this.props.file;
        let taskId = this.props.taskId;
        return (
            <div className={'row border rounded px-2 m-1'}>
                <a href={file.file_name}
                   className={'text-secondary text-bottom'}>
                    {file.original_name}
                </a>
                <button className={'close'}
                        onClick={() => deleteFile(file.file_name, taskId)}>
                    <i className={'bi bi-x'}>

                    </i>
                </button>
            </div>
        );
    }
}


